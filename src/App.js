import React, { Component } from "react";
import "./index.css";
// import todosList from "./todos.json";
import TodoList from "./TodoList.js";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { addTodo, clearCompletedTodo } from "./actions";

class App extends Component {
  state = {
    input: ""
  };

  handleChange = event => {
    console.log("Hey");
    this.setState({ input: event.target.value });
  };

  addTodo = event => {
    if (event.keyCode === 13 && event.target.value !== "") {
      this.props.addTodo(event.target.value);
      event.target.value = "";
    }
  };

  render() {
    return (
      <Router>
        <section className="todoapp">
          <header className="header">
            <h1>todos</h1>
            <input
              className="new-todo"
              placeholder="What needs to be done?"
              autoFocus
              value={this.input}
              onChange={this.handleChange}
              onKeyDown={this.addTodo}
            />
          </header>
          <Route
            exact
            path="/todoapp3"
            render={() => (
              <TodoList
                todos={this.props.todos}
                handleChange={this.handleChange}
              />
            )}
          />
          <Route
            path="/active"
            render={() => (
              <TodoList
                todos={this.props.todos.filter(todo => {
                  if (todo.completed === false) {
                    return todo;
                  }
                  return false;
                })}
                handleChange={this.handleChange}
              />
            )}
          />
          <Route
            path="/completed"
            render={() => (
              <TodoList
                todos={this.props.todos.filter(todo => {
                  if (todo.completed === true) {
                    return todo;
                  }
                  return false;
                })}
                handleChange={this.handleChange}
              />
            )}
          />
          <footer className="footer">
            <span className="todo-count">
              <strong>
                {
                  this.props.todos.filter(todo => {
                    if (todo.completed === false) {
                      return todo;
                    }
                    return false;
                  }).length
                }
              </strong>{" "}
              item(s) left
            </span>
            <ul className="filters">
              <li>
                <NavLink exact to="/todoapp3" activeClassName="selected">
                  All
                </NavLink>
              </li>
              <li>
                <NavLink to="/active" activeClassName="selected">
                  Active
                </NavLink>
              </li>
              <li>
                <NavLink to="/completed" activeClassName="selected">
                  Completed
                </NavLink>
              </li>
            </ul>
            <button
              className="clear-completed"
              onClick={this.props.clearCompletedTodo}
            >
              Clear completed
            </button>
          </footer>
        </section>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {
    todos: state.todos
  };
}

const mapDispatchToProps = {
  clearCompletedTodo,
  addTodo
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
