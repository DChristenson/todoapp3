import TodoItem from "./TodoItem.js";
import React, { Component } from "react";
import { deleteTodo, toggleTodo } from "./actions";
import { connect } from "react-redux";

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map(todo => (
            <TodoItem
              key={todo.id}
              title={todo.title}
              completed={todo.completed}
              toggleTodo={event => this.props.toggleTodo(todo)}
              deleteTodo={event => this.props.deleteTodo(todo)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

const mapDispatchToProps = {
  deleteTodo,
  toggleTodo
};

export default connect(
  null,
  mapDispatchToProps
)(TodoList);
