const ADD_TODO = "ADD_TODO";
const TOGGLE_TODO = "TOGGLE_TODO";
const DELETE_TODO = "DELETE_TODO";
const CLEAR_COMPLETED_TODOS = "CLEAR_COMPLETED_TODOS";

export const addTodo = payload => {
  return {
    type: ADD_TODO,
    payload
  };
};

export const toggleTodo = payload => {
  return {
    type: TOGGLE_TODO,
    payload
  };
};

export const deleteTodo = payload => {
  return {
    type: DELETE_TODO,
    payload
  };
};

export const clearCompletedTodo = payload => {
  return {
    type: CLEAR_COMPLETED_TODOS,
    payload
  };
};

export { ADD_TODO, TOGGLE_TODO, DELETE_TODO, CLEAR_COMPLETED_TODOS };
