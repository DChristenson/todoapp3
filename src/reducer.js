import {
  ADD_TODO,
  TOGGLE_TODO,
  DELETE_TODO,
  CLEAR_COMPLETED_TODOS
} from "./actions";

import todosList from "./todos.json";

let initialState = {
  todos: todosList,
  input: ""
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_TODO:
      let tempStateOne = state.todos.filter(todo => {
        if (todo.id === action.payload.id) {
          todo.completed = !todo.completed;
        }
        return todo;
      });
      return { ...state, todos: tempStateOne };

    case ADD_TODO:
      let tempState = state.todos.slice();

      tempState.push({
        id: Math.floor(Math.random() * 10000000),
        userID: 1,
        title: action.payload,
        completed: false
      });

      return { ...state, todos: tempState };

    case DELETE_TODO:
      const newTodos = state.todos.filter(
        item => item.id !== action.payload.id
      );
      return { ...state, todos: newTodos };

    case CLEAR_COMPLETED_TODOS:
      let tempTodos = state.todos.filter(todo => todo.completed === false);
      return { ...state, todos: tempTodos };

    default:
      return state;
  }
}

export default reducer;
